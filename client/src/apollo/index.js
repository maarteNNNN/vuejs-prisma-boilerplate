import Vue from 'vue'

// This is everything we need to work with Apollo 2.0.
import ApolloClient from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import VueApollo from 'vue-apollo'
import { ApolloLink } from 'apollo-link'

// config.headers['Authorization'] = 'Bearer ' + token
// Vue.http.headers.common['Authorization'] = token

// Create an Authorization header
const AuthLink = (operation, forward) => {
  // Fill in your own token
  // In the terminal you can request a token by: prisma token -c
  // And it copies it
  const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InNlcnZpY2UiOiJzZXJ2ZXJAZGV2Iiwicm9sZXMiOlsiYWRtaW4iXX0sImlhdCI6MTUxODIxMzE1NSwiZXhwIjoxNTE4ODE3OTU1fQ.ukoc_aImNPNyMEvWE-kw3BVc-dotrhD6mZ5CuTUxl1k'

  operation.setContext(context => ({
    ...context,
    headers: {
      ...context.headers,
      authorization: `Bearer ${token}`
    }
  }))

  return forward(operation)
}

// Create a new HttpLink to connect to your GraphQL API.
// According to the Apollo docs, this should be an absolute URI.
// Create the apollo client
const link = ApolloLink.from([
  AuthLink,
  new HttpLink({
    // You might want to use http://localhost:4000
    // I really don't know this about Prisma yet, since the docs have been doing very weird for me lately
    uri: 'http://localhost:4466/server/dev'
  })
])

// Create the apollo client
const apolloClient = new ApolloClient({
  // Tells Apollo to use the link chain with the http link we set up.
  link: link,
  // Handles caching of results and mutations.
  cache: new InMemoryCache(),
  // Handles caching of results and mutations.
  connectToDevTools: true
})

// Register the VueApollo plugin with Vue.
// Install the vue plugin
// With the apollo client instance
Vue.use(VueApollo)

const apolloProvider = new VueApollo({
  // Apollo 2.0 allows multiple clients to be enabled at once.
  // Here we select the default (and only) client.
  defaultClient: apolloClient
})

export default apolloProvider
