import Vue from 'vue'
import Router from 'vue-router'
import SampleUsersPage from '@/pages/SampleUsersPage'
import SampleUserPage from '@/pages/SampleUserPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'SampleUsersPage',
      component: SampleUsersPage
    },
    {
      path: '/user/:id',
      name: 'SampleUserPage',
      component: SampleUserPage
    }
  ]
})
